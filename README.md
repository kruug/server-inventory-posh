# Server Inventory PoSh

This script will query AD, find all entries with the Windows Server Operating System, and then query all services and installed programs.  The script will then write this data to a CSV file.

### Prerequisites

Requires PSLogging to be installed: https://www.powershellgallery.com/packages/PSLogging/
Needs to be run on a server with Active Directory, or RSAT tools installed.

## Built With

* [PowerShell ISE](https://docs.microsoft.com/en-us/powershell/scripting/components/ise/introducing-the-windows-powershell-ise?view=powershell-7) - IDE Used
* PowerShell 5.1.18362.145, Should work with any PowerShell 4 and above

## Authors

* **Viktor Kruug** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

Thank you to @chrisdent and the PowerShell Discord server for assisting and showing me the proper method of working in PowerShell.