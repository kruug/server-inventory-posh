﻿#requires -version 4
<#
.SYNOPSIS
  This script will query AD, find all entries with the Windows Server Operating System, and then query all services and installed
    programs.  The script will then write this data to a CSV file.

.DESCRIPTION
  This script will query AD, find all entries with the Windows Server Operating System, and then query all services and installed
    programs.  The script will then write this data to a CSV file.

.PARAMETER <Parameter_Name>
  <Brief description of parameter input required. Repeat this attribute if required>

.INPUTS
  None

.OUTPUTS Log File
  The script log file stored in C:\Windows\Temp\<Date in ISO format>-ServerInventory.log
  All other output files are stored in the same directory under their respective names.

.NOTES
  Version:        1.0
  Author:         Viktor Kruug
  Creation Date:  2020-01-28
  Purpose/Change: Initial script development

.DEPENDS
  Depends on PSLogging module, found here: https://www.powershellgallery.com/packages/PSLogging/

.EXAMPLE
  <Example explanation goes here>
  
  <Example goes here. Repeat this attribute for more than one example>
#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------

Param (
  #Script parameters go here
)

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Set Error Action to Silently Continue
$ErrorActionPreference = 'SilentlyContinue'

#Import Modules & Snap-ins
Import-Module PSLogging

#----------------------------------------------------------[Declarations]----------------------------------------------------------

#Script Version
$sScriptVersion = '1.0'

#Log File Info
$sLogPath = $env:APPDATA + '\ServerInventory'
$sLogName = $(get-date -f yyyy-MM-dd) + '-ServerInventory.log'
$sLogFile = Join-Path -Path $sLogPath -ChildPath $sLogName

#Output File Info
$sServerOutputName = $(get-date -f yyyy-MM-dd) + '-ADServers.csv'
$sSystemOutputName = $(get-date -f yyyy-MM-dd) + '-ADServersSystemInfo.csv'
$sServicesOutputName = $(get-date -f yyyy-MM-dd) + '-ADServersServicesInfo.csv'
$sSoftwareOutputName = $(get-date -f yyyy-MM-dd) + '-ADServersSoftwareInfo.csv'

$sServerOutputFile = Join-Path -Path $sLogPath -ChildPath $sServerOutputName
$sSystemOutputFile = Join-Path -Path $sLogPath -ChildPath $sSystemOutputName
$sServicesOutputFile = Join-Path -Path $sLogPath -ChildPath $sServicesOutputName
$sSoftwareOutputFile = Join-Path -Path $sLogPath -ChildPath $sSoftwareOutputName

#-----------------------------------------------------------[Functions]------------------------------------------------------------

<#
Function <FunctionName> {
  Param ()
  Begin {
    Write-LogInfo -LogPath $sLogFile -Message '<description of what is going on>...'
  }
  Process {
    Try {
      <code goes here>
    }
    Catch {
      Write-LogError -LogPath $sLogFile -Message $_.Exception -ExitGracefully
      Break
    }
  }
  End {
    If ($?) {
      Write-LogInfo -LogPath $sLogFile -Message 'Completed Successfully.'
      Write-LogInfo -LogPath $sLogFile -Message ' '
    }
  }
}
#>

Function Get-Servers {
  Param ()
  Begin {
    Write-LogInfo -LogPath $sLogFile -Message 'Creating CSV file of all Windows servers in AD...'
    Write-Host 'Creating CSV file of all Windows servers in AD'
  }
  Process {
    Try {
      Get-ADComputer -Filter {OperatingSystem -like "*windows*server*"} -Properties * | sort DNSHostname | select DNSHostName | ConvertTo-Csv  | % {$_ -replace '"',''} | Select-Object -Skip 2 | Out-File -filePath $sServerOutputFile
    }
    Catch {
      Write-LogError -LogPath $sLogFile -Message $_.Exception -ExitGracefully
    }
  }
  End {
    If ($?) {
      Write-LogInfo -LogPath $sLogFile -Message 'Completed Successfully.'
      Write-LogInfo -LogPath $sLogFile -Message ' '
      Write-Host 'CSV file created.'
    }
  }
}

Function Get-SystemInfo {
  Param ()
  Begin {
    Write-LogInfo -LogPath $sLogFile -Message 'Gathering system information...'
  }

  Process {
    Try {
      If(test-path $sSystemOutputFile) {
        Clear-Content $sSystemOutputFile
      }

      $servers = Foreach ($server in Get-Content $sServerOutputFile) {
        Write-LogInfo -LogPath $sLogFile -Message "Contacting $server..."
        Write-Host "Contacting $server..."
        Try {
          $sSession = New-CimSession -ComputerName $server
        }
        Catch {
          Write-Host 'Failed'
          Write-LogError -LogPath $sLogFile -Message $_.Exception 
        }
        
        #Run the commands concurrently for each server in the list
        Write-LogInfo -LogPath $sLogFile -Message "  Retrieving system information"
        Write-Host -NoNewLine "  Retrieving system information..."

        Try {
          $SystemInfo = Get-CimInstance -ClassName CIM_ComputerSystem -CimSession $sSession | Select-Object Name, Manufacturer, Model
          Write-Host 'Done'
        }
        Catch {
          Write-Host 'Failed'
          Write-LogError -LogPath $sLogFile -Message $_.Exception 
        }

        Write-LogInfo -LogPath $sLogFile -Message "  Retrieving Dell Service Tag"
        Write-Host -NoNewLine "  Retrieving Dell Service Tag..."
        Try {
          $ServiceTag = Get-CimInstance -ClassName Win32_BIOS -CimSession $sSession | Select-Object SerialNumber
          Write-Host 'Done'
        }
        Catch {
          Write-Host 'Failed'
          Write-LogError -LogPath $sLogFile -Message $_.Exception 
        }

        Write-LogInfo -LogPath $sLogFile -Message "  Retrieving CPU information"
        Write-Host -NoNewLine "  Retrieving CPU information..."
        Try {
          $CPUInfo = Get-CimInstance -ClassName Win32_Processor -CimSession $sSession | Select-Object Name, SocketDesignation
          Write-Host 'Done'
        }
        Catch {
          Write-Host 'Failed'
          Write-LogError -LogPath $sLogFile -Message $_.Exception 
        }

        Write-LogInfo -LogPath $sLogFile -Message "  Retrieving OS information"
        Write-Host -NoNewLine "  Retrieving OS information..."
        Try {
          $OSInfo = Get-CimInstance -ClassName CIM_OperatingSystem -CimSession $sSession | Select-Object Caption, Version
          Write-Host 'Done'
        }
        Catch {
          Write-Host 'Failed'
          Write-LogError -LogPath $sLogFile -Message $_.Exception 
        }

        #Get Memory Information. The data will be shown in a table as GB, rounded to the nearest second decimal.
        Write-LogInfo -LogPath $sLogFile -Message "  Retrieving installed RAM"
        Write-Host -NoNewLine "  Retrieving installed RAM..."
        Try {
          $PhysicalMemory = Get-CimInstance CIM_PhysicalMemory -CimSession $sSession | Measure-Object -Property capacity -Sum | % {[math]::round(($_.sum / 1GB),2)}
          Write-Host 'Done'
        }
        Catch {
          Write-Host 'Failed'
          Write-LogError -LogPath $sLogFile -Message $_.Exception 
        }

        #Get Network Configuration
        Write-LogInfo -LogPath $sLogFile -Message "  Retrieving network configuration"
        Write-Host -NoNewLine "  Retrieving network configuration..."
        Try {
          $Network = Get-CimInstance Win32_NetworkAdapterConfiguration -Filter 'ipenabled = "true"' -CimSession $sSession
          Write-Host 'Done'
        }
        Catch {
          Write-Host 'Failed'
          Write-LogError -LogPath $sLogFile -Message $_.Exception 
        }

        #Get local admins.
        Write-LogInfo -LogPath $sLogFile -Message "  Retrieving local admins"
        Write-Host -NoNewLine "  Retrieving local admins..."
        Try {
          $localadmins = Get-CimInstance -ClassName win32_group -Filter "name = 'administrators'" -ComputerName $server | Get-CimAssociatedInstance -Association win32_groupuser
          Write-Host 'Done'
        }
        Catch {
          Write-Host 'Failed'
          Write-LogError -LogPath $sLogFile -Message $_.Exception 
        }

        #Get list of shares
        Write-LogInfo -LogPath $sLogFile -Message "  Retrieving list of network shares"
        Write-Host -NoNewLine "  Retrieving list of network shares..."
        Try {
          $Shares = Get-CimInstance Win32_share -CimSession $sSession | Where {$_.name -NotLike "*$"}
          Write-Host 'Done'
        }
        Catch {
          Write-Host 'Failed'
          Write-LogError -LogPath $sLogFile -Message $_.Exception 
        }

        #Create infoObject and add data to it.
        [PSCustomObject]@{
          MachineName = $SystemInfo.Name
          ServiceTag = $ServiceTag.SerialNumber
          CPUName = $CPUInfo.Name
          TotalRAMGB = $PhysicalMemory
          OSName = $OSInfo.Caption
          OSVersion = $OSInfo.Version
          IPAddress = $Network.IPAddress -join '; '
          LocalAdmins = $localadmins.Caption
          SharesName = $Shares.Name
          SharesPath = $Shares.Path
        }

        #Write-LogInfo -LogPath $sLogFile -Message " "
      }
      
      $servers | Select-Object * -ExcludeProperty PSComputerName, RunspaceId, PSShowComputerName | Export-Csv -NoTypeInformation -Path $sSystemOutputFile
    }
    Catch {
      Write-Host 'Failed'
      Write-LogError -LogPath $sLogFile -Message $_.Exception -ExitGracefully
    }
  }
  End {
    If ($?) {
      Write-LogInfo -LogPath $sLogFile -Message '  Completed Successfully.'
      Write-LogInfo -LogPath $sLogFile -Message ' '
    }
  }
}

Function Get-Services {
  Param ()
  Begin {
    Write-LogInfo -LogPath $sLogFile -Message '  Retrieving List of Services...'
    Write-Host -NoNewLine '  Retrieving List of Services...'
  }
  Process {
    Try {
      $services = Foreach ($server in Get-Content $sServerOutputFile) {
        #Get all services that are not present when server is first installed
        Get-Service -ComputerName $server -Exclude "AJRouter", "ALG", "AppIDSvc", "Appinfo", "AppMgmt", "AppReadiness", "AppVClient", "AppXSvc", "AudioEndpointBuilder", "Audiosrv", "AxInstSV", "BFE", "BITS", "BrokerInfrastructure", "BTAGService", "BthAvctpSvc", "bthserv", "camsvc", "CaptureService_1b8c5bd", "cbdhsvc_1b8c5bd", "CcmExec", "CDPSvc", "CDPUserSvc_1b8c5bd", "CertPropSvc", "ClipSVC", "CmRcService", "COMSysApp", "ConsentUxUserSvc_1b8c5bd", "CoreMessagingRegistrar", "CryptSvc", "CscService", "CSFalconService", "DcomLaunch", "defragsvc", "DeviceAssociationService", "DeviceInstall", "DevicePickerUserSvc_1b8c5bd", "DevicesFlowUserSvc_1b8c5bd", "DevQueryBroker", "Dhcp", "diagnosticshub.standardcollector.service", "DiagTrack", "DmEnrollmentSvc", "dmwappushservice", "Dnscache", "DoSvc", "dot3svc", "DPS", "DsmSvc", "DsSvc", "Eaphost", "EFS", "embeddedmode", "EntAppSvc", "EventLog", "EventSystem", "fdPHost", "FDResPub", "FontCache", "FrameServer", "gpsvc", "GraphicsPerfSvc", "hidserv", "HvHost", "icssvc", "IKEEXT", "InstallService", "iphlpsvc", "KeyIso", "KPSSVC", "KtmRm", "LanmanServer", "LanmanWorkstation", "lfsvc", "LicenseManager", "lltdsvc", "lmhosts", "lpasvc", "lppsvc", "LSM", "MapsBroker", "mpssvc", "MSDTC", "MSiSCSI", "msiserver", "NcaSvc", "NcbService", "Netlogon", "Netman", "netprofm", "NetSetupSvc", "NetTcpPortSharing", "NgcCtnrSvc", "NgcSvc", "NlaSvc", "nsi", "PcaSvc", "PerfHost", "PhoneSvc", "PimIndexMaintenanceSvc_1b8c5bd", "pla", "PlugPlay", "PolicyAgent", "Power", "PrintNotify", "PrintWorkflowUserSvc_1b8c5bd", "ProfSvc", "PushToInstall", "QWAVE", "RasAuto", "RasMan", "RemoteAccess", "RemoteRegistry", "RmSvc", "RpcEptMapper", "RpcLocator", "RpcSs", "RSoPProv", "sacsvr", "SamSs", "SCardSvr", "ScDeviceEnum", "Schedule", "SCPolicySvc", "seclogon", "SecurityHealthService", "SEMgrSvc", "SENS", "Sense", "SensorDataService", "SensorService", "SensrSvc", "SessionEnv", "SgrmBroker", "SharedAccess", "ShellHWDetection", "shpamsvc", "smphost", "smstsmgr", "SNMPTRAP", "Spooler", "sppsvc", "SSDPSRV", "ssh-agent", "SstpSvc", "StateRepository", "stisvc", "StorSvc", "svsvc", "swprv", "SysMain", "SystemEventsBroker", "TabletInputService", "tapisrv", "TermService", "Themes", "TieringEngineService", "TimeBrokerSvc", "TokenBroker", "TrkWks", "TrustedInstaller", "tzautoupdate", "UALSVC", "UevAgentService", "UmRdpService", "UnistoreSvc_1b8c5bd", "upnphost", "UserDataSvc_1b8c5bd", "UserManager", "UsoSvc", "VaultSvc", "vds", "VGAuthService", "vmicguestinterface", "vmicheartbeat", "vmickvpexchange", "vmicrdv", "vmicshutdown", "vmictimesync", "vmicvmsession", "vmicvss", "vmvss", "VMwareCAFCommAmqpListener", "VMwareCAFManagementAgentHost", "VSS", "W32Time", "WaaSMedicSvc", "WalletService", "WarpJITSvc", "WbioSrvc", "Wcmsvc", "WdiServiceHost", "WdiSystemHost", "WdNisSvc", "Wecsvc", "WEPHOSTSVC", "wercplsupport", "WerSvc", "WiaRpc", "WinDefend", "WinHttpAutoProxySvc", "Winmgmt", "WinRM", "wisvc", "wlidsvc", "wmiApSrv", "WMPNetworkSvc", "WPDBusEnum", "WpnService", "WpnUserService_1b8c5bd", "WSearch", "wuauserv"
      }

      $services | Select-Object MachineName, Name, DisplayName, Status, StartType | Export-Csv -NoTypeInformation -Path $sServicesOutputFile

      Write-Host 'Done'
    }
    Catch {
      Write-Host 'Failed'
      Write-LogError -LogPath $sLogFile -Message $_.Exception -ExitGracefully
    }
  }
  End {
    If ($?) {
      Write-LogInfo -LogPath $sLogFile -Message '  Completed Successfully.'
      Write-LogInfo -LogPath $sLogFile -Message ' '
    }
  }
}

Function Get-Software {
  Param ()
  Begin {
    Write-LogInfo -LogPath $sLogFile -Message '  Retrieving List of Software...'
    Write-Host -NoNewLine '  Retrieving List of Software...'
  }
  Process {
    #Array of software included in a base install.
    $sSoftwareArray = @("Configuration Manager Client", "Microsoft Policy Platform", "VMware Tools", "VMware, Inc.")

    Try {
      $Software = Foreach ($server in Get-Content $sServerOutputFile) {
        #Get all software that are is present when server is first installed
        
        #Define the variable to hold the location of Currently Installed Programs
        $UninstallKey = ”SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall”
        
        #Create an instance of the Registry Object and open the HKLM base key
        $reg = [microsoft.win32.registrykey]::OpenRemoteBaseKey(‘LocalMachine’,$server)
        
        #Drill down into the Uninstall key using the OpenSubKey Method
        $regkey = $reg.OpenSubKey($UninstallKey)
        
        #Retrieve an array of string that contain all the subkey names
        $subkeys = $regkey.GetSubKeyNames()
        
        #Open each Subkey and use GetValue Method to return the required values for each
        foreach($key in $subkeys){
          $thisKey = $UninstallKey+”\\”+$key
          $thisSubKey = $reg.OpenSubKey($thisKey)
          
          If (!($sSoftwareArray -Contains $thisSubKey.GetValue(“DisplayName”))) {
            #Create infoObject and add data to it.
            [PSCustomObject]@{
              MachineName = $server
              DisplayName = $($thisSubKey.GetValue(“DisplayName”))
              DisplayVersion = $($thisSubKey.GetValue(“DisplayVersion”))
              Publisher = $($thisSubKey.GetValue(“Publisher”))
            }
          }
        }
      }

      $Software | Where-Object { $_.Display_Name } | Sort-Object -Property Display_Name | Export-Csv -NoTypeInformation -Path $sSoftwareOutputFile

      Write-Host 'Done'
    }
    Catch {
      Write-Host 'Failed'
      Write-LogError -LogPath $sLogFile -Message $_.Exception -ExitGracefully
    }
  }
  End {
    If ($?) {
      Write-LogInfo -LogPath $sLogFile -Message '  Completed Successfully.'
      Write-LogInfo -LogPath $sLogFile -Message ' '
    }
  }
}

#-----------------------------------------------------------[Execution]------------------------------------------------------------

If(!(test-path $sLogPath))
{
      New-Item -ItemType Directory -Force -Path $sLogPath > $null
}

Start-Log -LogPath $sLogPath -LogName $sLogName -ScriptVersion $sScriptVersion > $null
#Script Execution goes here

Get-Servers
Get-SystemInfo
Get-Services
Get-Software

Stop-Log -LogPath $sLogFile